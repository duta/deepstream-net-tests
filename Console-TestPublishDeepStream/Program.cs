﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeepStreamNet.Contracts;
using DeepStreamNet;

namespace Console_TestPublishDeepStream
{
    class Program
    {

        static void Main(string[] args)
        {

            TestDeepStream test = new TestDeepStream();
            test.ConnectDeepStreamAsync().Wait();
            test.PublishTest().Wait();

        }


        
    }

    public class TestDeepStream {

        DeepStreamClient client;

        public async Task ConnectDeepStreamAsync()
        {
            //connect to default deepstream instance
            client = new DeepStreamClient("localhost", 6020, "deepstream");

            if (await client.LoginAsync())
            {
                //Login was successful 
                Console.WriteLine("Success login deepstream");
            }

            ////Alternative with credentials
            //await client.LoginAsync("Username", "Password");

            // Close Connection to deepstream.io endpoint
            //client.Dispose();

        }

        public async Task PublishTest() {
            // Subscribe to Event 'test'
            var eventSubscription = await client.Events.SubscribeAsync("test", x => { Console.WriteLine(x); });
                        
            // Send 'Hello' to all Subscribers of Event 'test'
            client.Events.Publish("test", "Hello");

            // Send number '42' to all Subscribers of Event 'test'
            client.Events.Publish("test", 42);

            // Send object '{Property1="Hello", Property2=42}' to all Subscribers of Event 'test'
            client.Events.Publish("test", new { Property1 = "Hello", Property2 = 42 });

            // Unsubscribe from Event 'test'
            await eventSubscription.DisposeAsync();            
        }

    }

    

}
